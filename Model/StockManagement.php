<?php


namespace M21\CurentStock\Model;

class StockManagement
{

    protected $_productRepository;
    protected $_stockItemRepository;


    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository
    )
    {
        $this->_productRepository = $productRepository;
        $this->_stockItemRepository = $stockItemRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getStock($param)
    {
        if (is_array($param)) {
            $res = [];
            foreach ($param as $key => $sku) {
                $product = $this->getProductBySku($sku);
                $stock = $this->getProductStockById($product->getId());
                $res[] = [$sku => $stock->getQty()];
            }
            return $res;
        } else {
            $product = $this->getProductBySku($param);
            $stock = $this->getProductStockById($product->getId());
            return [$param => $stock->getQty()];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function postStock($param, $stock)
    {
//        $product = $this->getProductBySku($param);
//        $stock_ = $this->getProductStockById($product->getId());
//        $stock_->setQty($stock);
//        $stock_->save();
        return [$param, $stock];
    }

    private function getProductBySku($sku)
    {
        try {
            return $this->_productRepository->get($sku);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    private function getProductStockById($product_id)
    {
        try {
            return $this->_stockItemRepository->getStockItem($product_id);
        } catch (Exception $e) {
            echo   $e->getMessage();
        }

    }
}
