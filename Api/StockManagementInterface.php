<?php


namespace M21\CurentStock\Api;

interface StockManagementInterface
{

    /**
     * GET for Stock api
     * @param mixed $param
     * @return string
     */
    public function getStock($param);

    /**
     * POST for Stock api
     * @param string $param
     * @return string
     */
    public function postStock($param,$stock);
}
